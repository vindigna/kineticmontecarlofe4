program create_fields_file

Implicit none

 integer,parameter					:: N_time_slot =100
 double precision,parameter		:: h_i=-0.6d4			! Gauss
 double precision,parameter		:: h_f= 0.6d4			! Gauss 
 double precision	 				:: h_step, field_nominal, field_true
 integer							:: itime
 character(len=40)				:: directory_fields_values='field_data/'			! used to read nominal/true fields from file
 character(len=80)				:: filename_1,filename_in,filename_fields

 filename_fields=trim(adjustl(directory_fields_values))//'fields_prova.dat' 
 open(unit=1, file=filename_fields)

 h_step=dabs(h_f-h_i)/dble(N_time_slot)		! with h=0 in the range


 do itime=1,N_time_slot						

   field_nominal = dble(itime-1)*h_step + h_i + h_step*0.5
   field_true = field_nominal
   write(1,*)itime, field_nominal, field_true


 enddo ! itime loop 

 close(1)


end program
