module ComputationParameters
! DO NOT USE any of those names for local variables!
    Implicit none
    ! by removing these commons i get NaN
    COMMON GI,D,E,B40,B42,B44,B43,B66,mu_B
    COMMON gamma_0,g1,g2,B_resonance,T
    ! ----------------- Numerical constants ------------------
    REAL (Kind=8),parameter         :: pi = 3.141592653589793238462643d0
    REAL (Kind=8)                   :: mu_B = (9.274078d-5)/1.380662d0  	! Bohr magneton [Kelvin/Gauss]
    REAL (Kind=8),parameter         :: eps = 1.d-13
    REAL (Kind=8),parameter         :: rad = pi/180.d0
    REAL (Kind=8),parameter         :: cm_to_Kelvin = 1.9863d0/1.380662d0

    ! ----------------- System parameters --------------------  ! for the moment we them constant, as per Fe4
    INTEGER (Kind=4),parameter      :: number_of_spins = 10		! spin can potentially have a different tilting
    INTEGER (Kind=4),parameter      :: matrix_size = 11		    ! 2*S+1 we define this instead of the spin S

    ! ----------------- spin-Hamiltonian parameters:  da08d6f985747709d8b133c80d19f420a90531b3
    REAL (Kind=8)       		    :: GI = 2.d0
    REAL (Kind=8)                   :: D = -0.418d0*cm_to_Kelvin    ! 3*B20
    REAL (Kind=8)                   :: E = 1.6d-2*cm_to_Kelvin      ! B22
    REAL (Kind=8)                   :: B40 = 1.5d-5*cm_to_Kelvin
    REAL (Kind=8)                   :: B42 = 1.d-4
    REAL (Kind=8)                   :: B43 = 8.95d-5*cm_to_Kelvin
    REAL (Kind=8)                   :: B44 = 0.d0
    REAL (Kind=8)                   :: B66 = 6.d-5*cm_to_Kelvin     ! ACHTUNG NOT PHYSICAL

    ! ----------------- Transition-rate parameters -----------
    REAL (Kind=8)                   :: gamma_0 =  1.1563d3      ! [Hz/Kelvin^5]     spin-phonon
    REAL (Kind=8)                   :: g1 = 4.d-2               ! [Kelvin]          spin-phonon
    REAL (Kind=8)                   :: g2 = 4.d-2               ! [Kelvin]          spin-phonon
    CHARACTER(len=40)               :: tunnel_window = 'gaussian' ! 'rectangle_v_independent' / 'rectangle' removed
    REAL (Kind=8)                   :: B_resonance(matrix_size,matrix_size)     ! gaussian
    REAL (Kind=8)                   :: sigma_tunnel_0 =  5.d1     ! [Gauss] width gaussian +5 -> -5
    REAL (Kind=8)                   :: sigma_tunnel_1 =  5.d1     ! [Gauss] width gaussian +5 -> -4
    REAL (Kind=8)                   :: sigma_tunnel_2 =  5.d1     ! [Gauss] width gaussian +5 -> -3
    REAL (Kind=8)                   :: v_tunnel_0 = 4.d2          ! [Gauss/s] peak gaussian +5 -> -5
    REAL (Kind=8)                   :: v_tunnel_1 = 6.d2          ! [Gauss/s] peak gaussian +5 -> -4
    REAL (Kind=8)                   :: v_tunnel_2 = 6.d2          ! [Gauss/s] peak gaussian +5 -> -3
    ! incoherence_factor_0 = 0 [LZ], incoherence_factor_0 = 1 [fully incoherent tunneling]
    REAL (Kind=8)                   :: incoherence_factor_0 = 1.d0
    REAL (Kind=8)                   :: incoherence_factor_1 = 1.d0
    REAL (Kind=8)                   :: incoherence_factor_2 = 1.d0

    ! ----------------- Simulation parameters ----------------
    INTEGER (Kind=4),parameter      :: N_time_slot = 2500 !5340! 8928 !  890*6        ! number of slots in which the elapsed time is discretized
    REAL (Kind=8)                   :: v_sweep = 0.2809d4   ! sweeping rate external field [Gauss/s]
    REAL (Kind=8)	                :: h_i = -1.25d4 ! -1.25d3/4 		    ! initial field [Gauss]
    REAL (Kind=8)		            :: h_f = 1.25d4  ! 1.25d3/4  	         ! final field [Gauss]
    REAL (Kind=8)                   :: T = 3.d-1
    REAL (Kind=8)			 	    :: theta_0 = 0.d0			! tilting easy axis w.r.t. normal to the plane in deg
    REAL (Kind=8)	                :: phi_0 = 0.d0
    INTEGER (Kind=4),parameter      :: iter_max = 400 ! 1000           ! Number of MC sweeps, iterations
    INTEGER (Kind=4) 	            :: iwrite = 10              ! partial averages are written in the output file every "iwrite" iterations
    INTEGER (Kind=4),parameter      :: seed = 980759715 		! random numbers seed
    INTEGER (Kind=4),parameter      :: p_initial = 1			! Initial state for each spin

    ! ----------------- logical parameters --------------------
    logical                         :: save_eig_and_W=.true.
    logical 					    :: random_phi=.false.
    logical 					    :: heat_bath=.false.
    logical 					    :: read_fields=.false.
    logical 					    :: average_critical_field =.false.

contains

    ! --------------------------------------------------------

    subroutine show_consts()
        print*, "Pi = ", pi
        print*,  "e = ", e
    end subroutine show_consts

end module ComputationParameters