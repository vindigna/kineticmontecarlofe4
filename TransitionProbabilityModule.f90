Module TransitionProbabilities
    USE SpinAlgebra
    USE ComputationParameters
    USE SpinHamiltonian
Contains

    SUBROUTINE transition_rates(H_x, H_y, H_z, W)
        implicit none

        INTEGER (Kind = 4) :: I, p, q, mm, jm(matrix_size)
        REAL (Kind = 8) :: H_x, H_y, H_z
        REAL (Kind = 8) :: W(matrix_size, matrix_size)
        COMPLEX (Kind = 8) :: S_proj_out(3, matrix_size)
        ! - diag routine ZHEEV
        REAL (Kind = 8) :: eig(matrix_size)
        COMPLEX (Kind = 8) :: HamMat(matrix_size, matrix_size)
        COMPLEX (Kind = 8), allocatable :: WORK(:), RWORK(:)
        INTEGER (Kind = 4) :: LWORK, INFO

        call Hamiltonian(H_x, H_y, H_z, HamMat)

        LWORK = 363 ! 2*matrix_size
        allocate(WORK(LWORK), RWORK(3 * matrix_size - 2))

        call zheev('V', 'U', matrix_size, HamMat, matrix_size, eig, WORK, LWORK, RWORK, INFO)
        !        write(*,*)'info = ', INFO
        !        write(*,*)'optimal LWORK = ', WORK(1)

        deallocate(WORK, RWORK)

        call PhysicalLabeling(HamMat, eig)

        do q = 1,matrix_size
            do p = 1, matrix_size
                !                Fermi golden's rule W(i -> f) \prop |<f| ... |i>|^2
                W(q, p) = transition_rate(eig, HamMat, H_x, H_y, H_z, p, q)
            end do
        end do

        call flush(6)

        RETURN
    END

    !---------------------------------------------------------

    function transition_rate(eigenVal, eigenVect, H_x, H_y, H_z, p, q)
        ! <p| ... |q> W(i -> f) ~ |<f| ... |i>|^2
        ! |p> = |f>
        ! |q> = |i>
        implicit none
        INTEGER (Kind = 4)  :: p, q
        REAL (Kind = 8)     :: H_x, H_y, H_z
        COMPLEX (Kind = 8)  :: eigenVect(matrix_size, matrix_size)
        REAL (Kind = 8)     :: eigenVal(matrix_size)
        REAL (Kind = 8)     :: S_plus_sq_sq, S_minus_sq_sq, S_plus_S_z_sq, S_minus_S_z_sq
        REAL (Kind = 8)     :: spin_phonon, thermal_weight, deltaE
        REAL (Kind = 8)     :: transition_rate ! output
        !----
        INTEGER (Kind = 4) :: m
        REAL (Kind = 8) :: S, Ms
        COMPLEX (Kind = 8) :: sum, arg
        REAL (Kind = 8) :: coeff

        if (p .eq. q) then
            transition_rate = 0.d0
        else

            S_plus_sq_sq = modSquare(S_plus_sq(eigenVect, p, q))
            S_minus_sq_sq = modSquare(S_minus_sq(eigenVect, p, q))
            S_plus_S_z_sq = S_plus_S_z(eigenVect, p, q)**2.
            S_minus_S_z_sq = S_minus_S_z(eigenVect, p, q)**2.

            spin_phonon = g2*g2*(S_plus_sq_sq + S_minus_sq_sq) + g1*g1*(S_plus_S_z_sq + S_minus_S_z_sq)
            spin_phonon = gamma_0 * spin_phonon

            deltaE = eigenVal(p) - eigenVal(q)
            thermal_weight = (deltaE**3.) / (dexp(deltaE / T) - 1.d0)

            if (abs(deltaE) / T.lt.40.d0) then
                thermal_weight = (deltaE**3.) / (dexp(deltaE / T) - 1.d0)
            else
                if (deltaE.gt.0.) then
                    ! limit T -> 0 for absorption transitions
                    thermal_weight = 0.d0
                else
                    ! limit T -> 0 for emission transitions
                    thermal_weight = -deltaE**3.
                endif
            endif

            transition_rate =  spin_phonon * thermal_weight

            !     -----    INTRODUCE PURE TUNNELING CHANNEL  ----
            SELECT CASE (tunnel_window)
            CASE ('gaussian')
                transition_rate = transition_rate + gamma_tunnel_gaussian_Fe4(H_x, H_y, H_z, p, q)
            CASE DEFAULT
                ! no tunneling window is added
            END SELECT

        end if ! q .ne. p condition

    end function transition_rate


    !---------------------------------------------------------

    function gamma_tunnel_gaussian_Fe4(H_x, H_y, H_z, p, q)
        ! <p| ... |q> W(i -> f) ~ |<f| ... |i>|^2
        ! |p> = |f>
        ! |q> = |i>
        ! ---- ACHTUNG ad hoc for Fe4!!!
        implicit none
        INTEGER (Kind = 4) :: p, q, i, j
        REAL (Kind = 8) :: H_x,H_y,H_z,H_ref,v_tunnel,sigma_tunnel,incoherence_factor
        REAL (Kind = 8) :: arg
        logical :: coherent_tunneling    ! +5 -> [other states]
        logical :: incoherent_tunneling  ! [other states] -> +5
        REAL (Kind = 8) :: gamma_tunnel_gaussian_Fe4 ! out

        ! initialization
        coherent_tunneling = .false.
        incoherent_tunneling = .false.
        sigma_tunnel = 0.d0
        v_tunnel = 0.d0
        incoherence_factor = 1.d0

        ! ----- +5 -> -5
        if (p == basis_index(-5.d0) .and. q == basis_index(5.d0))then
            coherent_tunneling = .true.
            v_tunnel = v_tunnel_0
            sigma_tunnel = sigma_tunnel_0
        elseif (p == basis_index(5.d0) .and. q == basis_index(-5.d0))then
            incoherent_tunneling = .true.
            incoherence_factor = incoherence_factor_0
            v_tunnel = v_tunnel_0
            sigma_tunnel = sigma_tunnel_0
            ! ----- +5 -> -4
        elseif(p == basis_index(-4.d0) .and. q == basis_index(5.d0))then
            coherent_tunneling = .true.
            v_tunnel = v_tunnel_1
            sigma_tunnel = sigma_tunnel_1
        elseif(p == basis_index(5.d0) .and. q == basis_index(-4.d0))then
            incoherent_tunneling = .true.
            incoherence_factor = incoherence_factor_1
            v_tunnel = v_tunnel_1
            sigma_tunnel = sigma_tunnel_1
            ! ----- -5 -> +4
        elseif (p == basis_index(4.d0) .and. q == basis_index(-5.d0))then
            coherent_tunneling = .true.
            v_tunnel = v_tunnel_1
            sigma_tunnel = sigma_tunnel_1
        elseif (p == basis_index(-5.d0) .and. q == basis_index(4.d0))then
            incoherent_tunneling = .true.
            incoherence_factor = incoherence_factor_1
            v_tunnel = v_tunnel_1
            sigma_tunnel = sigma_tunnel_1
            ! ----- +5 -> -3
        elseif(p == basis_index(-3.d0) .and. q == basis_index(5.d0))then
            coherent_tunneling = .true.
            v_tunnel = v_tunnel_2
            sigma_tunnel = sigma_tunnel_2
        elseif(p == basis_index(5.d0) .and. q == basis_index(-3.d0))then
            incoherent_tunneling = .true.
            incoherence_factor = incoherence_factor_2
            v_tunnel = v_tunnel_2
            sigma_tunnel = sigma_tunnel_2
            ! ----- -5 -> +3
        elseif (p == basis_index(3.d0) .and. q == basis_index(-5.d0))then
            coherent_tunneling = .true.
            v_tunnel = v_tunnel_2
            sigma_tunnel = sigma_tunnel_2
        elseif (p == basis_index(-5.d0) .and. q == basis_index(3.d0))then
            incoherent_tunneling = .true.
            incoherence_factor = incoherence_factor_2
            v_tunnel = v_tunnel_2
            sigma_tunnel = sigma_tunnel_2

        end if


        if(coherent_tunneling)then
            H_ref = dsqrt(H_x*H_x + H_y*H_y + H_z*H_z)*H_z/abs(H_z)
            arg = (H_ref - B_resonance(p,q))/sigma_tunnel
            gamma_tunnel_gaussian_Fe4 = dexp(-arg*arg*0.5)
            gamma_tunnel_gaussian_Fe4 = gamma_tunnel_gaussian_Fe4*v_tunnel/(sigma_tunnel*dsqrt(2.*pi))
        elseif(incoherent_tunneling)then
            H_ref = dsqrt(H_x*H_x + H_y*H_y + H_z*H_z)*H_z/abs(H_z)
            arg = (H_ref - B_resonance(p,q))/sigma_tunnel
            gamma_tunnel_gaussian_Fe4 = dexp(-arg*arg*0.5)
            gamma_tunnel_gaussian_Fe4 = gamma_tunnel_gaussian_Fe4*v_tunnel/(sigma_tunnel*dsqrt(2.*pi))
            gamma_tunnel_gaussian_Fe4 = gamma_tunnel_gaussian_Fe4*incoherence_factor
        else
            gamma_tunnel_gaussian_Fe4 = 0.d0
        end if

    end function gamma_tunnel_gaussian_Fe4


    !---------------------------------------------------------

    subroutine locate_resonance_fields_Fe4(H_dir_x,H_dir_y,H_dir_z, h_epsilon)
        implicit none
        INTEGER (Kind = 4)  :: p, q
        REAL (Kind = 8)     :: H_dir_x, H_dir_y, H_dir_z, h_step, h_epsilon

        B_resonance(:,:) = 0.d0

        p = basis_index(5.d0)
        q = basis_index(-5.d0)
        B_resonance(p,q) = resonance_field(5.d0,-5.d0,H_dir_x,H_dir_y,H_dir_z,2.d2,-5.d2,h_epsilon)

        p = basis_index(-4.d0)
        q = basis_index(5.d0)
        B_resonance(p,q) = resonance_field(-4.d0,5.d0,H_dir_x,H_dir_y,H_dir_z,2.d2,0.d0,h_epsilon)

        p = basis_index(4.d0)
        q = basis_index(-5.d0)
        B_resonance(p,q) = resonance_field(4.d0,-5.d0,H_dir_x,H_dir_y,H_dir_z,-2.d2,0.d0,h_epsilon)

        p = basis_index(-3.d0)
        q = basis_index(5.d0)
        B_resonance(p,q) = resonance_field(-3.d0,5.d0,H_dir_x,H_dir_y,H_dir_z,2.d2,0.d0,h_epsilon)

        p = basis_index(3.d0)
        q = basis_index(-5.d0)
        B_resonance(p,q) = resonance_field(3.d0,-5.d0,H_dir_x,H_dir_y,H_dir_z,-2.d2,0.d0,h_epsilon)

    end subroutine locate_resonance_fields_Fe4


    !---------------------------------------------------------

    function resonance_field(mi,mf,H_dir_x,H_dir_y,H_dir_z,h_step,h_initial,h_epsilon)
        implicit none
        INTEGER (Kind = 4) :: ii,p,q
        logical         :: sup
        REAL (Kind = 8) :: mi, mf
        REAL (Kind = 8) :: H_dir_x, H_dir_y, H_dir_z, h_step, h_inc, h_initial, h_epsilon
        REAL (Kind = 8) :: field, H_x, H_y, H_z
        REAL (Kind = 8) :: resonance_field ! out
        ! - diag routine ZHEEV
        REAL (Kind = 8) :: eig(matrix_size)
        COMPLEX (Kind = 8) :: HamMat(matrix_size, matrix_size)
        COMPLEX (Kind = 8), allocatable :: WORK(:), RWORK(:)
        INTEGER (Kind = 4) :: LWORK, INFO
        REAL (Kind = 8) :: deltaE,deltaE_min

        p = basis_index(mi)
        q = basis_index(mf)

        LWORK = 363 ! 2*matrix_size
        allocate(WORK(LWORK), RWORK(3 * matrix_size - 2))

        field = h_initial
        h_inc = h_step
        deltaE_min = 10.d0
        deltaE = 0.d0

        do while (deltaE .lt. 10.)

            field =  field + h_inc

            H_x = H_dir_x*field
            H_y = H_dir_y*field
            H_z = H_dir_z*field

            call Hamiltonian(H_x, H_y, H_z, HamMat)
            call zheev('V', 'U', matrix_size, HamMat, matrix_size, eig, WORK, LWORK, RWORK, INFO)
            !            write(*,*)'info = ', INFO
            !            write(*,*)'optimal LWORK = ', WORK(1)
            call PhysicalLabeling(HamMat, eig)
            deltaE = dabs(eig(p)-eig(q))

            if (deltaE > deltaE_min) then
                if (dabs(h_inc) < h_epsilon) then
                    exit
                else
                    field =  field - 2.*h_inc
                    h_inc = h_inc/5.
                    deltaE_min = 10.
                end if
            else
                deltaE_min = deltaE
            end if
        end do

        resonance_field = field

        deallocate(WORK, RWORK)

    end function resonance_field

    !---------------------------------------------------------

    pure function modSquare(cc)
        implicit none
        COMPLEX (Kind = 8), intent(in) :: cc
        REAL (Kind = 8) :: modSquare
        modSquare = abs(cc) * abs(cc)
    end function modSquare

    !---------------------------------------------------------

End Module TransitionProbabilities