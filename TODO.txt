- make input parameters readable from external (csv?) files without the need to recompile the project
- create a simulation label to be readout from input filename and attached to output files
- create a type resonance: |m_i>, |m_f>, sigma_tunnel, B_resonance, alpha_tunnel
- append resonances to a list  of resonance types
- Use matrix_size as global parameter and eliminate Nd [DONE]
- Try to eliminate the commons [it produces some errors!]

After meeting 2.10.2020
- introduce parameter coherence_factor  From LZ to incoherent tunneling
- express t_max in terms of DeltaH and v_sweep
