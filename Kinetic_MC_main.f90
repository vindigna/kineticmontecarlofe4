! © 2020 ETH Zurich, [PD Dr. Alessandro Vindigni]
include 'ParametersModule.f90'
include 'SpinAlgebraModule.f90'
include 'SpinHamiltonianModule.f90'
include 'FileNamesModule.f90'
include 'RandomModule.f90'
include 'VariablesMainModule.f90'
include 'TransitionProbabilityModule.f90' ! comment this line out to indent this file with IntelliJ
include 'TestModule.f90'

program Kinetic_MC_main
    USE ComputationParameters
    USE SpinAlgebra
    USE SpinHamiltonian
    USE TransitionProbabilities
    USE FileNames
    USE Tests
    USE Randomize
    USE VariablesMain

    call cpu_time(start)

    print '(/"kinetic MonteCarlo Fe4"/)'

    ! RND number generator
    S = spinModulus()
    mmm = mzranset(521288629,362436069,16163801,seed)

    h_step=dabs(h_f-h_i)/dble(N_time_slot)		! with h=0 in the range
    time_max = dabs(h_f-h_i)/v_sweep
    t_inc=time_max/dble(N_time_slot)

    theta=rad*theta_0
    phi = rad*phi_0

    write(*,'("# type of tunnel_window: " (A) )') tunnel_window

    SELECT CASE (tunnel_window)
    CASE ('gaussian')
        ! this case is handled in the TransitionProbabilityModule.f90
        call locate_resonance_fields_Fe4(dsin(theta)*dcos(phi),&
                dsin(theta)*dsin(phi), dcos(theta),1.d-4)
        do i = 1,matrix_size
            do j = 1, matrix_size
                if(B_resonance(j,i) .ne.0.d0) write(*,*) j,i,B_resonance(j,i)
            end do
        end do
    CASE DEFAULT
        ! gamma_tunnel = 0.d0 ! not needed
    END SELECT

    if(.NOT.read_fields) then
        print '("t_inc = " (e12.4) x "[s]")', t_inc
        !        print '("h_step = " (F8.4) x "[T]")', (1d-4)*h_step
        print '("h_step = " (F8.4) x "[Gauss]")', h_step
        print '("v_sweep = " (e12.4) x "[T/s]")', (1d-4)*v_sweep
    endif

    print '("v_tunnel_0 = " (e12.4) x "[T/s]")', v_tunnel_0*1d-4
    !tmp_h = 6.62607015d-11/(2.*pi*1.380662d0)
    !print '("h_bar = " (e12.4) x "[Kelvin*s]")', tmp_h
    !tmp = 8.14d-5 ! tunnel splitting in K
    !print '("tunnel splitting = " (e12.4) x "[Kelvin]")', tmp
    !v_0 = 2.*tmp*tmp/(tmp_h*GI*mu_B*S)
    !print '("v_LZ = " (e12.4) x "[T/s]")', v_0*1d-4
    ! comparing v_sweeep with v_0 it should be clear that
    ! realistic cases correspond to the the adiabatic LZ limit in which v_0 >> v_sweep
    ! =>  this justifies our phenomenological approach

    !    stop

    write(*,*)

    i=1
    WW(:,:,:)=0.d0

    if(read_fields)open(unit=2, file=filename_fields)

    ! centered in the interval
    do itime=1,N_time_slot

        if( read_fields ) then
            read(2,*)ih, field_nominal, field_true
            field_values(itime)=field_true
        else
            field_values(itime)=dble(itime-1)*h_step + h_i + h_step*0.5
        endif

    enddo ! itime loop

    if(read_fields)close(2)

    tmp_h =  h_f
    ih = (tmp_h-h_i)/h_step
    ih = ih + 1

    print '(" time max = " (F12.4) x "[s]"/)', time_max

    ! write the eigenvalues and transition rates in a separate files
    if (save_eig_and_W) then
        open(unit=1, file=filename_eigenvalues)
        open(unit=2, file=filename_transition_rates)
        phi = rad*phi_0
        call labelsEnergyLevels(matrix_size,theta,phi,N_time_slot,field_values)
        close(1)
        close(2)
    end if

    !==================  MAIN BLOCK ====================================

    call random_num_generator	! intialize the seed for rnd number generator

    Prob(:,:)=0.d0
    Norm(:)=0.d0

    do iter=1,iter_max

        ! --- define transition rates differently for phi_rnd or not
        do itime=1,N_time_slot
            if(random_phi) then
                rnd_phi = random()
                phi = pi*rnd_phi
            else
                phi = rad*phi_0
            endif

            H_x = dsin(theta)*dcos(phi)*field_values(itime)
            H_y = dsin(theta)*dsin(phi)*field_values(itime)
            H_z = dcos(theta)*field_values(itime)	! ACHTUNG field_values(itime)

            ! transition probabilities spin-phonon taken from the book "Molecular Nanomagnets" Gatteschi-Sessoli-Villain
            call transition_rates(H_x,H_y,H_z,W_single)

            do q=1,matrix_size
                do p=1,matrix_size
                    if(p.ne.q)WW(p,q,itime)=W_single(p,q)
                enddo
            enddo

        enddo   		!   itime transition rates

        call MonteCarlo_sweep(Prob,Norm)

        !----  write file
        if(mod(iter,iwrite)==0) then
            write(*,*)'write temportary output iter = ',iter
            call write_current_averages
        end if

    enddo !iter

    write(*,*)
    call cpu_time(finish)
    write(*,*)'CPU time [seconds]= ',finish-start


contains

    !---------------------------------------------------------------------

    subroutine Init_ferro
        INTEGER (Kind=4)	      :: ii

        do ii=1,number_of_spins
            conf(ii) = p_initial
        enddo

    end Subroutine Init_ferro

    !---------------------------------------------------------------------

    pure function Boltzmann_weight(TT,eigenVal)
        implicit none
        REAL (Kind=8),intent(in)    :: eigenVal(matrix_size),TT
        REAL (Kind=8)               :: Boltzmann_weight(matrix_size) ! output
        INTEGER (Kind=4)            :: pp
        REAL (Kind=8)               :: zeta_part

        zeta_part=0.
        do pp=1,matrix_size
            zeta_part = zeta_part + dexp(-eigenVal(pp)/TT)
        enddo
        do pp=1,matrix_size
            Boltzmann_weight(pp) = dexp(-eigenVal(pp)/TT)/zeta_part
        enddo
    end function

    !---------------------------------------------------------------------

    subroutine mag_equilibrium_z
        REAL (Kind=8)		:: Proj_eq,zeta_sum
        REAL (Kind=8)       :: P_eq(matrix_size)

        zeta_sum=0.
        do p=1,matrix_size
            zeta_sum = zeta_sum + dexp(-En_levels(p)/T)
        enddo

        do p=1,matrix_size
            P_eq(p)=dexp(-En_levels(p)/T)/zeta_sum
        enddo

        Proj_eq = 0.d0
        do p=1,matrix_size
            Proj_eq = Proj_eq - S_proj(3,p)*P_eq(p)
        enddo
        Mag_eq =  Proj_eq/S

    end subroutine mag_equilibrium_z

    !---------------------------------------------------------------------

    Subroutine Advance_time_squares(ii,t_i,q_final,Delta_t_min)

        INTEGER (Kind=4)	:: q_final,ii,q_max
        REAL (Kind=8)		:: tmp_int
        REAL (Kind=8)		:: delta_tt,control,rr,h_mod,tmp_min,control_min
        REAL (Kind=8)		:: int_tt(matrix_size),log_rr(matrix_size),t_step(matrix_size)
        REAL (Kind=8)		:: t_i,tt,Delta_t_min,W_max

        ! Implementation of the First-Reaction-Algorithm with discretized, "squarelike" transition rates.

        p = conf(ii)
        W_max = 0.d0
        do q=1,matrix_size

            rr = random()
            log_rr(q)=-dlog(rr)

            W_single(p,q)=WW(p,q,itime)

            if(W_single(p,q)>W_max)then
                W_max=W_single(p,q)
                q_max=q
            endif

        enddo !q loop

        int_tt(:)=0.
        t_step(:)=time_max

        do q=1,matrix_size

            tmp_int=WW(p,q,itime)*(dble(itime)*t_inc -t_i)

            if(tmp_int .ge. log_rr(q))then
                t_step(q)=log_rr(q)/WW(p,q,itime)
            else
                int_tt(q) = tmp_int
                ih = itime
                do while(int_tt(q) < log_rr(q))

                    ih = ih + 1
                    if(ih > N_time_slot) then
                        t_step(q)=time_max
                        exit
                    endif

                    tmp_int = int_tt(q) + WW(p,q,ih)*t_inc

                    if(tmp_int > log_rr(q))then
                        t_step(q) = (log_rr(q)-int_tt(q))/WW(p,q,ih)
                        t_step(q) = t_step(q) + (ih-1)*t_inc - t_i
                        exit
                    else
                        int_tt(q) = tmp_int
                    endif

                enddo ! while

            endif ! integral condition

        enddo ! q loop

        rr = random()

        q_final = 1 + matrix_size*rr

        Delta_t_min = time_max ! - t_i

        do q=1,matrix_size
            if(t_step(q) < Delta_t_min)then
                Delta_t_min = t_step(q)
                q_final=q
            endif
        enddo

        if(Delta_t_min<1.d-13)then
            write(*,*)'routine Achtung Delta_t_min<0!!!',Delta_t_min,q_max,log_rr(q_max)/W_max
            q_final=q_max
            Delta_t_min=log_rr(q_max)/W_max

        endif

    End Subroutine Advance_time_squares


    !------------------------------------------------------------

    subroutine MonteCarlo_sweep(PP,NN)

        REAL (Kind=8)   :: PP(number_of_spins*matrix_size,N_time_slot),NN(N_time_slot)
        REAL (Kind=8)	:: h_ext_x,h_ext_y,h_ext_z

        time = 0.d0
        Delta_t = time_max -time

        h_ext_x = dsin(theta)*dcos(phi)*field_values(1)
        h_ext_y = dsin(theta)*dsin(phi)*field_values(1)
        h_ext_z = dcos(theta)*field_values(1)

        !--- initialization
        call Init_ferro
        !-----------------------------------
        i=1
        time = 0.d0
        time_old = 0.d0
        itime = 1
        itime_old = 1
        icount = 0


        do while (i < number_of_spins*10 )	! A condition which is always fulfilled!

            ! the spin which would make the transition within the shortest tiem is the one which is actually flipped => i = ir

            iadvance=0
            time_left = time_max -time
            Delta_t = time_left

            do ir=1,number_of_spins

                call Advance_time_squares(ir,time,qr,Delta_t_local)

                if(Delta_t_local < Delta_t)then
                    iadvance = iadvance+1
                    Delta_t=Delta_t_local
                    i=ir
                    q_i=qr
                endif

            enddo

            if(iadvance==0)Delta_t = time_max + 1.	! no event has happened

            if(Delta_t<1.d-13)write(*,*)time,'achtung Delta_t=0!',Delta_t,'iadvance=',iadvance
            ! Advance time and define probability

            time_old = time
            itime_old = itime
            time = time + Delta_t

            tmp_h = h_i + time*h_step/t_inc
            itime=int(time/t_inc)  + 1   			                ! N.B. Delta_t is allowed to be larger than t_inc

            if(itime == itime_old) then
                icount = icount + 1
                NN(itime) = NN(itime) + Delta_t 			        ! it has to be outside the loop on the lattice

                do ir=1,number_of_spins
                    p = conf(ir)	! 1 + (sigma(ir) + 1)/2			! vector of the states in which the each spin is found
                    ip = (ir-1)*matrix_size + p 					! label site + level before flipping
                    PP(ip,itime) = PP(ip,itime) + Delta_t		    ! probabilities are stored with global time
                enddo

            else

                if(mod(iter,iwrite)==0.and.mod(itime,10)==0)write(*,*)'iter=',iter,'itime=', itime, icount

                icount = 0

                dt_f = itime_old*t_inc - time_old			! the slice of delta t in the final time slot

                NN(itime_old) = NN(itime_old) + dt_f	    ! it has to be outside the loop on the lattice

                it_max=itime						        ! used for updating intermediate time slots
                if(itime > N_time_slot)then
                    it_max = N_time_slot
                endif

                do ir=1,number_of_spins

                    p = conf(ir)
                    ip = (ir-1)*matrix_size + p

                    PP(ip,itime_old) = PP(ip,itime_old) + dt_f

                    !	update intermediate time slots
                    do it = itime_old+1,it_max
                        dt = time - (it-1)*t_inc
                        if (dt>t_inc)dt = t_inc
                        PP(ip,it) = PP(ip,it) + dt
                        if(ir==1)NN(it) = NN(it) + dt
                    enddo

                enddo ! ir loop to define

            endif ! close (itime == itime_old)


            ! flip the spin if not time > time_max

            !-------------------------------------------
            if (time < time_max)then
                conf(i) = q_i
            else
                exit ! do while
            endif

            !	END UPDATE CONFIGURATION
            !-------------------------------------------

            if (itime > N_time_slot)exit	! do while => this happens also when Delta_t = 0, the following one not

        enddo   ! do while

    end subroutine MonteCarlo_sweep

    !------------------------------------------------------------

    subroutine write_current_averages

        open(unit=1, file=filename_magnetization)
        open(unit=3, file=filename_computation_parameters)
        if(read_fields) open(unit=2, file=filename_fields)

        !----------- output header ------------------
        if ( average_critical_field )then
            do i_skip = 1, (N_time_slot + 2)*icritical
                read(1,*)
            enddo
        else
            write(1,*)'H_nominal, ', 'H_true, ' ,'M_stoch, ','M_eq, ','time,', 'itime'
        endif
        !--------------------------------------------


        time_check = 0.d0
        do itime=1,N_time_slot

            if( read_fields ) then
                read(2,*)ih, field_nominal, field_true
                field_values(itime)=field_true
            elseif ( average_critical_field )then
                field_nominal = dble(itime-1)*h_step + h_i + h_step*0.5
                field_true =  field_values(itime)
            else
                field_true = field_values(itime)
                field_nominal = field_true
            endif

            Population(:,:)=0.d0

            do ir=1,number_of_spins
                if(Norm(itime)>1.d-14)then
                    do p=1,matrix_size
                        ip = (ir-1)*matrix_size + p !label site + level
                        Population(ir,p)=Prob(ip,itime)/Norm(itime)     ! needed in dipolar_fields
                    enddo
                endif
            enddo ! lattice sweep

            h_ext_x = dsin(theta)*dcos(phi)*field_values(itime)
            h_ext_y = dsin(theta)*dsin(phi)*field_values(itime)
            h_ext_z = dcos(theta)*field_values(itime)

            Mag_eq_average = 0.d0
            Mag(:) = 0.d0
            Proj(:) = 0.d0
            icount=0

            Population_states(:)=0.d0

            do ir=1,number_of_spins

                H_x = h_ext_x
                H_y = h_ext_y
                H_z = h_ext_z

                call spin_projections(H_x,H_y,H_z,En_levels,S_proj)

                if(Norm(itime)>1.d-14)then
                    icount=icount+1
                    do p=1,matrix_size
                        Proj(1) = Proj(1) - S_proj_real(S_proj(1,p))*Population(ir,p)
                        Proj(2) = Proj(2) - S_proj_real(S_proj(2,p))*Population(ir,p)
                        Proj(3) = Proj(3) - S_proj_real(S_proj(3,p))*Population(ir,p)

                        Population_states(p) = Population_states(p) + Population(ir,p)
                    enddo
                endif 		!Norm condition

                time_check = time_check + Norm(itime)

                call mag_equilibrium_z		! actung it is computed locally

                Mag_eq_average = Mag_eq_average +  Mag_eq

            enddo ! ir loop

            Mag_eq_average = Mag_eq_average/dble(number_of_spins)

            Proj(1)=Proj(1)/(S*real(icount, kind=8))
            Proj(2)=Proj(2)/(S*real(icount, kind=8))
            Proj(3)=Proj(3)/(S*real(icount, kind=8))

            Mag_stoch = dsin(theta)*dcos(phi)*Proj(1)
            Mag_stoch = Mag_stoch + dsin(theta)*dsin(phi)*Proj(2)
            Mag_stoch = Mag_stoch + dcos(theta)*Proj(3)

            if (average_critical_field) then
                write(1,*) field_critical, field_nominal, field_true, Mag_stoch, Mag_eq_average, t_inc*itime,itime
            else
                write(1,'(5(g12.4 ",")i4)')field_nominal, field_true, Mag_stoch, Mag_eq_average, t_inc*itime,itime
            endif

            if(itime==1)write(*,*)icritical ,t_inc*itime,field_values(itime),Proj(3),Mag_stoch,Mag_eq_average


        enddo ! itime

        if(read_fields)close(2)

        write(*,*)'time_check =',time_check

        if (average_critical_field) then
            write(1,*)
            write(1,*)
        else

            !----------- output ending ------------------
            write(3,'("# Source code: Kinetic_MC_main.f90")')
            write(3,'("#")')
            write(3,'("# System parameters")')
            write(3,'("# number_of_spins = " (I5))') number_of_spins
            write(3,'("# spin states 2*S+1 = " (I2))') matrix_size
            write(3,'("#")')
            write(3,'("# Spin Hamiltonian parameters")')
            write(3,'("# g Lande = " (e12.4) x "[K]")') GI
            write(3,'("# D = " (e12.4) x "[K]")') D
            write(3,'("# E = " (e12.4) x "[K]")') E
            write(3,'("# B40 = " (e12.4) x "[K]")') B40
            write(3,'("# B42 = " (e12.4) x "[K]")') B42
            write(3,'("# B43 = " (e12.4) x "[K]")') B43
            write(3,'("# B44 = " (e12.4) x "[K]")') B44
            write(3,'("# B66 = " (e12.4) x "[K]")') B66
            write(3,'("#")')
            write(3,'("# Phonon-assisted ransition-rate parameters")')
            write(3,'("# gamma_0 = " (e12.4) x "[Hz/K^5]")') gamma_0
            write(3,'("# spin-phonon g1 = " (e12.4) x "[K]")') g1
            write(3,'("# spin-phonon g2 = " (e12.4) x "[K]")') g2
            write(3,'("#")')
            write(3,'("# Resonance tunnel windows parameters")')
            write(3,'("# type of tunnel_window: " (A) )') tunnel_window
            write(3,'("# sigma_tunnel_0 = " (e12.4) x "[Gauss]")') sigma_tunnel_0
            write(3,'("# sigma_tunnel_1 = " (e12.4) x "[Gauss]")') sigma_tunnel_1
            write(3,'("# sigma_tunnel_2 = " (e12.4) x "[Gauss]")') sigma_tunnel_2
            write(3,'("# v_tunnel_0 = " (e12.4) x "[Gauss/s]")') v_tunnel_0
            write(3,'("# v_tunnel_1 = " (e12.4) x "[Gauss/s]")') v_tunnel_1
            write(3,'("# v_tunnel_2 = " (e12.4) x "[Gauss/s]")') v_tunnel_2
            write(3,'("# incoherence_factor_0 = " (e12.4) x "[pure number]")') incoherence_factor_0
            write(3,'("# incoherence_factor_1 = " (e12.4) x "[pure number]")') incoherence_factor_1
            write(3,'("# incoherence_factor_2 = " (e12.4) x "[pure number]")') incoherence_factor_2
            write(3,'("#")')
            write(3,'("# Physical simulation parameters")')
            write(3,'("# N_time_slot = " (I5) x "[pure number]")') N_time_slot
            write(3,'("# v_sweep = " (e12.4) x "[T/s] to be compared, e.g., with the intrinsic LZ velocity")') 1d-4*v_sweep
            write(3,'("# time_max = " (e12.4) x "[s]")') time_max
            write(3,'("# t_inc = " (e12.4) x "[s]")') t_inc
            write(3,'("# h_i = " (e12.4) x "[Gauss]")') h_i
            write(3,'("# h_f = " (e12.4) x "[Gauss]")') h_f
            write(3,'("# T = " (e12.4) x "[K]")') T
            write(3,'("# tilting easy axis theta_0 = " (e12.4) x "[deg]")') theta_0
            write(3,'("# tilting easy axis phi_0 = " (e12.4) x "[deg]")') phi_0
            write(3,'("# h_crit superconductor = " (e12.4) x "[Gauss]")') h_crit_Pb
            write(3,'("#")')
            write(3,'("# Computational simulation parameters")')
            write(3,'("# number of MC sweeps, iter_max = " (I5) x "[K]")') iter_max
            write(3,'("# iwrite = " (I5) x "[K]")') iwrite
            write(3,'("# random sequence seed = " (I10) x "[K]")') seed
            write(3,'("# p_initial = " (I2) x "[K]")') p_initial
            write(3,'("#")')
            write(3,'("# Control parameters")')
            write(3,'("# h_step = " (F8.4) x "[Gauss] to be compared, e.g., with sigma_tunnel")') h_step
            ! write(3,'("# intrinsic LZ velocity v_LZ = " (e12.4) x "[T/s] to be compared, e.g., with the intrinsic LZ velocity")')  v_0*10-4
            !--------------------------------------------

        endif ! write ending

        flush(1)
        close(1)
        flush(3)
        close(3)

    end subroutine write_current_averages

end program Kinetic_MC_main