Module SpinHamiltonian
    USE ComputationParameters
    USE SpinAlgebra
Contains

    subroutine PhysicalLabeling(eigenVect, eig)

        implicit none

        logical :: empty(matrix_size)
        REAL (Kind = 8) :: component_sq, sum
        REAL (Kind = 8) :: eig(matrix_size), eigRoutine(matrix_size), proj_max(matrix_size)
        COMPLEX (Kind = 8) :: eigenVect(matrix_size, matrix_size), eigenVectRoutine(matrix_size, matrix_size)
        INTEGER (Kind = 4) :: I, J, JM(matrix_size), II

        empty(:) = .true.  ! initialization
        ! we assume that |m = 0> has always the highest energy: it can be checked with J = MAXLOC(eig, matrix_size)
        JM(11) = 6
        empty(6) = .false.

        do J = 1, matrix_size - 1 ! i exclude the last one already assigned
            proj_max(J) = 0.
            do I = 1, matrix_size
                if(empty(I))then
                    component_sq = dconjg(eigenVect(I, J)) * eigenVect(I, J)
                    if(component_sq>proj_max(J))then
                        proj_max(J) = component_sq
                        JM(J) = I
                    endif
                endif
            enddo
            II = JM(J)
            empty(II) = .false.
        enddo

        eigRoutine(:) = eig(:)
        eigenVectRoutine(:, :) = eigenVect(:, :)

        do J = 1, matrix_size
            I = JM(J)
            eig(I) = eigRoutine(J)    ! reorder the eigenvalues according to max proj on |m>
            do II = 1, matrix_size
                eigenVect(II, I) = eigenVectRoutine(II, J)    ! reorder the egenvectors
            enddo
        enddo

    end subroutine PhysicalLabeling

    !---------------------------------------------------------

    SUBROUTINE spin_projections(H_x, H_y, H_z, eig, S_proj_out)
        implicit none

        INTEGER (Kind = 4) :: I, p, q, mm, jm(matrix_size)
        REAL (Kind = 8) :: H_x, H_y, H_z
        COMPLEX (Kind = 8) :: S_proj_out(3, matrix_size)
        ! - diag routine ZHEEV
        REAL (Kind = 8) :: eig(matrix_size)
        COMPLEX (Kind = 8) :: HamMat(matrix_size, matrix_size)
        COMPLEX (Kind = 8), allocatable :: WORK(:), RWORK(:)
        INTEGER (Kind = 4) :: LWORK, INFO

        call Hamiltonian(H_x, H_y, H_z, HamMat)

        LWORK = 363 ! 2*matrix_size
        allocate(WORK(LWORK), RWORK(3 * matrix_size - 2))

        call zheev('V', 'U', matrix_size, HamMat, matrix_size, eig, WORK, LWORK, RWORK, INFO)
        !        write(*,*)'info = ', INFO
        !        write(*,*)'optimal LWORK = ', WORK(1)

        deallocate(WORK, RWORK)

        call PhysicalLabeling(HamMat, eig)

        do q = 1,matrix_size
            ! spin components
            S_proj_out(1,q) = (0.5d0,0.d0)*(S_plus(HamMat, q, q) + S_minus(HamMat, q, q))
            S_proj_out(2,q) = (0.d0,-0.5d0)*(S_plus(HamMat, q, q) - S_minus(HamMat, q, q))
            S_proj_out(3,q) = S_z(HamMat, q, q)
        end do

        call flush(6)

        RETURN
        END subroutine spin_projections

    !---------------------------------------------------------

    SUBROUTINE ZEEMAN(H_x, H_y, H_z, DAT)
        implicit none
        COMPLEX (Kind = 8) :: DAT(matrix_size, matrix_size)
        REAL (Kind = 8), allocatable :: Z(:)
        COMPLEX (Kind = 8) :: C_x, C_y, C_z
        COMPLEX (Kind = 8), parameter :: PR = (1.d0, 0.d0)
        COMPLEX (Kind = 8), parameter :: PI = (0.d0, 1.d0)
        REAL (Kind = 8) :: H_x, H_y, H_z, SP
        INTEGER (Kind = 4) :: I, J, IS1, IDF

        C_x = H_x * PR
        C_y = H_y * PI
        C_z = H_z * PR
        do I = 1, matrix_size
            do J = I, matrix_size
                DAT(I, J) = (0.00, 0.00)
                IDF = J - I
                IF(Idf.eq.0) DAT(I, I) = mu_B * GI * C_z * S_z_coeff(I)

                ! note that by convention S_z_coeff = S + 1 - J therefore S_plus actually decreases |J> by one unit
                IF(IDF.eq.1) then
                    DAT(I, J) = GI * mu_B * (C_x - C_y) * 5.d-1 * S_plus_coeff(J)
                endif
                DAT(J, I) = CONJG(DAT(I, J))
            enddo
        enddo
        RETURN
    END subroutine ZEEMAN

    !---------------------------------------------------------

    subroutine Hamiltonian(H_x, H_y, H_z, Ham)
        implicit none
        REAL (Kind = 8) :: MAT(matrix_size, matrix_size)
        COMPLEX (Kind = 8) :: ZEE(matrix_size, matrix_size)
        COMPLEX (Kind = 8), parameter :: PR = (1.d0, 0.d0)
        COMPLEX (Kind = 8) :: Ham
        dimension :: Ham(matrix_size, matrix_size)
        REAL (Kind = 8) :: S_sq, S
        REAL (Kind = 8) :: H_x, H_y, H_z
        INTEGER (Kind = 4) :: I, J, IS1, IDF

        S = spinModulus()
        S_sq = S * (S + 1.)

        ! I have changed the convention so that M(I,J) = <I|...|J>
        ! reference: http://easyspin.org/easyspin/documentation/stevensoperators.html
        ! note that by convention S_z_coeff = S + 1 - J therefore S_plus actually decreases |J> by one unit

        MAT = 0.d0
        do I = 1, matrix_size
            ! diagonal terms
            MAT(I, I) = D * (S_z_coeff(I)**2) + 35. * B40 * (S_z_coeff(I)**4) - &
                    B40 * (30.*S_sq - 25.)*(S_z_coeff(I)**2)
            J = I + 2
            if(J .le. matrix_size)then
                MAT(I, J) = E * 0.5 * S_plus_coeff(J-1)*S_plus_coeff(J)
                MAT(I, J) = MAT(I, J) + 0.25 * B42 * S_plus_coeff(J-1)*S_plus_coeff(J) * &
                        (7. * S_z_coeff(J)**2 + 7. * S_z_coeff(J-2)**2  - 2.*(S - 5.))
            end if

            J = I + 3
            if(J .le. matrix_size)then
                MAT(I, J) = MAT(I, J) + 0.25 * B43 * S_plus_coeff(J-2)*S_plus_coeff(J-1)* &
                        S_plus_coeff(J) * (S_z_coeff(J) + S_z_coeff(J-3))
            end if

            J = I + 4
            if(J .le. matrix_size)then
                MAT(I, J) = MAT(I, J) + 0.5 * B44 * S_plus_coeff(J-3)*S_plus_coeff(J-2)* &
                        S_plus_coeff(J-1)*S_plus_coeff(J)
            end if

            J = I + 6
            if(J .le. matrix_size)then
                MAT(I, J) = MAT(I, J) + 0.5 * B66 * S_plus_coeff(J-5)*S_plus_coeff(J-4)* &
                        S_plus_coeff(J-3)*S_plus_coeff(J-2)*S_plus_coeff(J-1)*S_plus_coeff(J)
            end if
        end do

        call ZEEMAN(H_x, H_y, H_z, ZEE)
        do I = 1, matrix_size
            do J = I, matrix_size
                Ham(I, J) = (0.d0, 0.d0)
                Ham(I, J) = MAT(I, J) * PR + ZEE(I, J)
                Ham(J, I) = DCONJG(Ham(I, J))
            end do
        end do

        return

    end subroutine Hamiltonian


End Module SpinHamiltonian