program create_average

Implicit none

 integer,parameter			:: N_time_slot =400
double precision	 			:: h_crit_Pb = 7.d2
 double precision	 			:: field_critical, field_true, Mag_eq_average, time, h_ext, effe_sc, effe_normal, Mag_average
 double precision	 		 	:: Mag_stoch_normal(N_time_slot), Mag_stoch_sc(N_time_slot), field_nominal(N_time_slot)
 integer						:: itime, ii, iblock
 character(len=40)				:: directory='June_2019/'
 character(len=80)				:: filename_out,filename_in,filename_fields

filename_in=trim(adjustl(directory))//'Fe4_d2.dat'
filename_out=trim(adjustl(directory))//'Fe4_d2_average.dat'

 open(unit=1, file=filename_in)
 open(unit=2, file=filename_out)

 do iblock = 0,1
   do itime=1,N_time_slot					
     if(iblock == 0)read(1,*) field_critical, field_nominal(itime), field_true, Mag_stoch_normal(itime), Mag_eq_average, time,ii
     if(iblock == 1)read(1,*) field_critical, field_nominal(itime), field_true, Mag_stoch_sc(itime), Mag_eq_average, time,ii
   enddo ! itime loop 

   read(1,*)
   read(1,*)
enddo 


 do itime=1,N_time_slot					
    h_ext =  field_nominal(itime) 

    
    effe_sc = 1. -  dabs(h_ext)/h_crit_Pb 
    if (effe_sc < 0.d0 ) effe_Sc = 0.d0   
    effe_normal = 1. - effe_sc
    write(*,*) field_nominal(itime), Mag_stoch_normal(itime),Mag_stoch_sc(itime), effe_sc, effe_normal
    Mag_average = Mag_stoch_normal(itime)*effe_normal +  Mag_stoch_sc(itime)*effe_sc
    write(2,*) field_nominal(itime), Mag_average, Mag_stoch_normal(itime), Mag_stoch_sc(itime)
     
 enddo ! itime loop 


  ! write(1,*)itime, field_nominal, field_true



 close(1)
 close(2)

end program
