makefile_Fe4: Kinetic_MC_main.f90 ParametersModule.f90 FileNamesModule.f90 TransitionProbabilityModule.f90
	gfortran rnd_subroutine.f90 Kinetic_MC_main.f90 -llapack -lblas -o Fe4
#	gfortran rnd_subroutine.f90 Kinetic_MC_main.f90 -llapack -lblas -o Fe4 -fcheck=bounds  # to DEBUG malloc()
#	gfortran rnd_subroutine.f90 Kinetic_MC_main.f90 -llapack -lblas -o Fe4 -ffpe-trap=invalid,overflow,underflow,zero
